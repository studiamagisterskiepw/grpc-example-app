# gRPC-Example-app

Aplikacja stworzona w celu przykładu użycia frameworka gRPC.

W celu uruchomienia aplikacji należy:
1. Wybudować pliki za pomocą mavena. W celu wygenerowania klas z pliku o rozszerzeniu .proto
2. Uruchomić aplikację znajdującą się w paczce server...
3. Uruchomić aplikację znajdującą się w paczce client...
4. Prześledzić logi aplikacji w celu zaobserowania komunikacji.