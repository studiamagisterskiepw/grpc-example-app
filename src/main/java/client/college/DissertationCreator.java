package client.college;

import com.google.protobuf.ByteString;
import grpc.Dissertation;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DissertationCreator {

  public Dissertation queryDissertationCreator() throws IOException {
    ClassLoader classLoader = DissertationCreator.class.getClassLoader();

    File file = new File(classLoader.getResource("PrzykladowaPracaDyplomowa.txt").getFile());
    byte[] bytesArray = new byte[(int) file.length()];
    FileInputStream fis = new FileInputStream(file);
    fis.read(bytesArray);
    fis.close();

    return Dissertation.newBuilder()
        .setFirstName("Przykladowy")
        .setLastName("Student")
        .setTopic("Temat Pracy dyplomowej")
        .setFile(ByteString.copyFrom(bytesArray))
        .build();
  }
}
