package client.college;

import grpc.AntiPlagiarismServiceGrpc;
import grpc.AntiPlagiarismStatus;
import grpc.Dissertation;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.io.IOException;

public class GrpcCollege {

  public static void main(String[] args) throws IOException {
    ManagedChannel channel =
        ManagedChannelBuilder.forAddress("localhost", 8080).usePlaintext().build();
    AntiPlagiarismServiceGrpc.AntiPlagiarismServiceBlockingStub stub =
        AntiPlagiarismServiceGrpc.newBlockingStub(channel);
    Dissertation dissertation = new DissertationCreator().queryDissertationCreator();
    AntiPlagiarismStatus antiPlagiarismStatus = stub.checkAntiPlagiarism(dissertation);

    System.out
        .println("Czy praca dyplomowa jest plagiatem?: " + antiPlagiarismStatus.getIsPlagiarism());

    channel.shutdown();
  }
}
