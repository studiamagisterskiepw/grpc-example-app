package server.anti.plagiarism.system;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;

public class GrpcAntiPlagiarismSystem {

  public static void main(String[] args) throws IOException, InterruptedException {
    Server server = ServerBuilder.forPort(8080)
        .addService(new AntiPlagiarismService()).build();

    System.out.println("Uruchamiam serwer...");
    server.start();
    System.out.println("Uruchomiony!");
    server.awaitTermination();
  }
}
