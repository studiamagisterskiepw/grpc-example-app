package server.anti.plagiarism.system;

import grpc.AntiPlagiarismServiceGrpc.AntiPlagiarismServiceImplBase;
import grpc.AntiPlagiarismStatus;
import grpc.Dissertation;
import io.grpc.stub.StreamObserver;
import java.util.Random;

public class AntiPlagiarismService extends AntiPlagiarismServiceImplBase {

  @Override
  public void checkAntiPlagiarism(
      Dissertation dissertation, StreamObserver<AntiPlagiarismStatus> responseObserver) {
    System.out.println("Zapytanie od klienta:\n" + dissertation);
    AntiPlagiarismStatus antiPlagiarismStatus =
        AntiPlagiarismStatus.newBuilder().setIsPlagiarism(new Random().nextBoolean()).build();

    responseObserver.onNext(antiPlagiarismStatus);
    responseObserver.onCompleted();
  }
}
